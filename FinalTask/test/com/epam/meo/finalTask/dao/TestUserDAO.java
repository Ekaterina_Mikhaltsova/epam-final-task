package com.epam.meo.finalTask.dao;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import com.epam.meo.finalTask.exception.DAOException;
import com.epam.meo.finalTask.exception.PoolException;

public class TestUserDAO {

	private static UserDAO userDao;
	
	@BeforeClass
	public static void init() throws PoolException {
		userDao = new UserDAO();
	}
	
	@Test
	public void testCheck() throws DAOException, PoolException {
		assertEquals(false, userDao.check("kate"));
	}
	
	@Test
	public void testSearch() throws DAOException, PoolException {
		assertEquals(3, userDao.search("kate"));
	}
}
