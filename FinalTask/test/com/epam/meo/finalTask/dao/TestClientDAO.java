package com.epam.meo.finalTask.dao;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import com.epam.meo.finalTask.exception.DAOException;
import com.epam.meo.finalTask.exception.PoolException;

public class TestClientDAO {

	private static ClientDAO clientDao;
	
	@BeforeClass
	public static void init() throws PoolException {
		clientDao = new ClientDAO();
	}
	
	@Test
	public void testSearch() throws DAOException, PoolException {
		assertEquals(2, clientDao.search("kate"));
	}

}
