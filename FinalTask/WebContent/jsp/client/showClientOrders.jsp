<%@ taglib uri="/WEB-INF/tag/customTag.tld" prefix="ct" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList"%>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="properties/messages" />

<html>

<head>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<title><fmt:message key="bought"/></title>
</head>

<body>
	<p align="right"><ct:today format="dd.MM.yyyy"/></p>
	<p><fmt:message key="you.are"/> ${user}</p>
	<p align="center"><fmt:message key="orders"/></p>
	<table border="3" align="center">
	<tr>
	    <th><fmt:message key="id"/></th>
	    <th><fmt:message key="contract.number"/></th>
	    <th><fmt:message key="date"/></th>
	    <th><fmt:message key="status"/></th>
	    <th><fmt:message key="tour.id"/></th>
	    <th><fmt:message key="manager"/></th>
	</tr>
	<c:forEach var="orders" items="${orders}">
	    <tr>
	        <td><a>${orders.id}</a></td>
	        <td><a>${orders.contractNumber}</a></td>
	        <td><a>${orders.date}</a></td>
	        <td><a>${orders.status}</a></td>
	        <td><a>${orders.tourId}</a></td>
	        <td><a>${orders.manager}</a></td>
	        <td>
	        	<form name="buyForm" method="POST" action="controller">
					<input type="hidden" name="command" value="deleteOrder"/>
					<input type="hidden" name="orderid" value="${orders.id}"/>
					<input type="submit" value="<fmt:message key="button.delete"/>"/>
				</form>
			</td>
	    </tr>
	</c:forEach>
	</table>
	<form name="buyForm" method="POST" action="controller">
		<p align="right">
		<input type="hidden" name="command" value="mainClient"/>
		<input type="submit" value="<fmt:message key="main"/>"/>
		</p>
	</form>
	<form name="logOutForm" method="POST" action="controller">
		<p align="right">
		<input type="hidden" name="command" value="logout"/>
		<input type="submit" value="<fmt:message key="button.logout"/>"/>
		</p>
	</form>
</body>
</html>