<%@ taglib uri="/WEB-INF/tag/customTag.tld" prefix="ct" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="properties/messages" />

<html>

<head>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<title><fmt:message key="main"/></title>
</head>

<body>
	<p align="right"><ct:today format="dd.MM.yyyy"/></p>
	<p align="center">${user}, <fmt:message key="hello"/>!</p>
	<hr/><br>
	<table align="center">
	<tr>
	<td>
	<form name="mainClientForm" method="POST" action="controller">
		<p align="center">
		<input type="hidden" name="command" value="showTours"/>
		<input type="submit" class="sendsubmit" value=""/>
		<br><fmt:message key="button.show.tours"/><br><br><br>
		</p>
	</form>
	</td>
	<td>
	<form name="mainClientForm" method="POST" action="controller">
		<p align="center">
		<input type="hidden" name="command" value="showClientOrders"/>
		<input type="submit" class="delete" value=""/>
		<br><fmt:message key="button.show.orders"/><br><br><br>
		</p>
	</form>
	</td>
	</tr>
	</table>
	<form name="logOutForm" method="POST" action="controller">
		<p align="right">
		<input type="hidden" name="command" value="logout"/>
		<input type="submit" value="<fmt:message key="button.logout"/>"/>
		</p>
	</form>
</body>
</html>