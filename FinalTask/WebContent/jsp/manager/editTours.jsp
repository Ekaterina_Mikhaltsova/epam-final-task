<%@ taglib uri="/WEB-INF/tag/customTag.tld" prefix="ct" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList"%>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="properties/messages" />

<html>

<head>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<title><fmt:message key="edit"/></title>
</head>

<body>
	<p align="right"><ct:today format="dd.MM.yyyy"/></p>
	<p align="center"><fmt:message key="tours.table"/></p>
	<table border="3" align="center">
	<tr>
	    <th><fmt:message key="id"/></th>
	    <th><fmt:message key="price"/>, $</th>
	    <th><fmt:message key="date"/></th>
	    <th><fmt:message key="length"/></th>
	    <th><fmt:message key="discount"/>, %</th>
	    <th><fmt:message key="hot"/></th>
	    <th><fmt:message key="type"/></th>
	</tr>
	<c:forEach var="tours" items="${tours}">
	    <tr>
	        <td><a>${tours.id}</a></td>
	        <td><a>${tours.price}</a></td>
	        <td><a>${tours.date}</a></td>
	        <td><a>${tours.length}</a></td>
	        <td><a>${tours.discount}</a></td>
	        <td><a>${tours.isHot()}</a></td>
	        <td><a>${tours.type}</a></td>
	        <td>
	        	<form name="buyForm" method="POST" action="controller">
					<input type="hidden" name="command" value="editTours"/>
					<input type="hidden" name="tourid" value="${tours.id}"/>
					<input type="submit" value="<fmt:message key="button.edit"/>"/>
				</form>
			</td>
	    </tr>
	</c:forEach>
	</table>
	<form name="logOutForm" method="POST" action="controller">
		<p align="right">
		<input type="hidden" name="command" value="logout"/>
		<input type="submit" value="<fmt:message key="button.logout"/>"/>
		</p>
	</form>
</body>
</html>