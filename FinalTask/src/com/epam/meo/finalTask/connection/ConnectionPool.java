package com.epam.meo.finalTask.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;

import com.epam.meo.finalTask.exception.PoolException;

/**
 * Class implements connection pool .
 * <p>
 * Provides users with connections, quantity of which is limited.
 * </p>
 * 
 * @author Kate
 *
 */
public class ConnectionPool {

	private static BlockingQueue<Connection> pool = new ArrayBlockingQueue<Connection>(10, true);
	private static ResourceBundle resource = ResourceBundle.getBundle("properties.database");
	private static String url = resource.getString("db.url");
	private static String user = resource.getString("db.user");
	private static String pass = resource.getString("db.password");
    private static ConnectionPool instance;
    private static int connectionQuantity = Integer.parseInt(resource.getString("db.poolsize"));
 
    private Logger logger = Logger.getLogger(this.getClass());
    
    /**
     * Realization of Singleton pattern, there can be only one pool in the system.
     * 
     * @return instance
     * 
     * @throws SQLException 
     * @throws PoolException
     */
	public synchronized static ConnectionPool getInstance() throws SQLException, PoolException {
        if (instance == null) {
            instance = new ConnectionPool();
        }
        return instance;
    } 
	
	/**
     * Constructor in which JDBC driver gets set up.
     * 
     * @throws PoolException  If there is no JDBC driver or there are problems with the adding. 
     */
    public ConnectionPool() throws PoolException {
    	try {
		    Class.forName(resource.getString("db.driver")).newInstance();
		} catch (Exception e) {
			logger.error(e.getMessage());
			PoolException pool = new PoolException(e.getMessage());
            pool.setPropertyMessage("error.pool.exception");
            throw pool;
		}
        for (int i = 0; i < connectionQuantity; i++) {
            try {
                pool.add(DriverManager.getConnection(url, user, pass));
            } catch (SQLException e) {
            	logger.error(e.getMessage());
            	PoolException pool = new PoolException(e.getMessage());
	            pool.setPropertyMessage("error.pool.exception");
	            throw pool;
            }
        }
    }
 
    /**
     * Returns {@link Connection} object if any is available in the queue.
     * 
     * @return conn
     * 
     * @throws PoolException  If there is a problem with taking a {@link Connection} object from the queue.
     */
    public Connection getConnection() throws PoolException {
        Connection conn = null;
        try {
            conn = pool.take();
        } catch (InterruptedException e) {
        	logger.error(e.getMessage());
        	PoolException pool = new PoolException(e.getMessage());
            pool.setPropertyMessage("error.pool.exception");
            throw pool;
        }
        return conn;
    }
 
    
    /**
     * Returns {@link Connection} object in the queue, making it available for use.
     * 
     * @param conn  the returning {@link Connection} object
     * 
     * @throws PoolException  If there is a problem with putting a {@link Connection} object to the queue.
     */
    public void closeConnection(Connection conn) throws PoolException {
        if (conn != null) {
            try {
                pool.put(conn);
            } catch (InterruptedException e) {
            	logger.error(e.getMessage());
            	PoolException pool = new PoolException(e.getMessage());
	            pool.setPropertyMessage("error.pool.exception");
	            throw pool;
            }
        }
    }
}
