package com.epam.meo.finalTask.logic;

import java.util.List;

import com.epam.meo.finalTask.dao.OrderDAO;
import com.epam.meo.finalTask.entity.Order;
import com.epam.meo.finalTask.exception.DAOException;
import com.epam.meo.finalTask.exception.PoolException;

public class OrderLogic {

	public static boolean add(int tourId, int clientId) throws DAOException, PoolException {
		OrderDAO orderDao = new OrderDAO();
		if (orderDao.add(tourId, clientId)) {
			return true;
		} else {
			return false;
		}
	}
	
	public static List<Order> showOrders(int clientId) throws DAOException, PoolException {
        OrderDAO orderDao = new OrderDAO();
        List<Order> orders = orderDao.find(clientId);
        return orders;
    }
	
	public static boolean delete(int orderId) throws DAOException, PoolException {
        OrderDAO orderDao = new OrderDAO();
        if (orderDao.delete(orderId)) {
			return true;
		} else {
			return false;
		}
    }
}
