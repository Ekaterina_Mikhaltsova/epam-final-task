package com.epam.meo.finalTask.logic;

import com.epam.meo.finalTask.dao.UserDAO;
import com.epam.meo.finalTask.exception.DAOException;
import com.epam.meo.finalTask.exception.PoolException;

public class UserLogic {

	public static int search(String name) throws DAOException, PoolException {
		UserDAO userDao = new UserDAO();
		return userDao.search(name);
	}
}
