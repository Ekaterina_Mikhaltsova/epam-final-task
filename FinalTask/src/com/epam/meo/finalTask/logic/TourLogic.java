package com.epam.meo.finalTask.logic;

import java.util.List;

import com.epam.meo.finalTask.dao.TourDAO;
import com.epam.meo.finalTask.entity.Tour;
import com.epam.meo.finalTask.exception.DAOException;
import com.epam.meo.finalTask.exception.PoolException;

public class TourLogic {

	public static List<Tour> showTours() throws DAOException, PoolException {
        TourDAO tourDao = new TourDAO();
        List<Tour> tours = tourDao.readAll();
        return tours;
    } 
	
	public static Tour search(int id) throws PoolException, DAOException {
		TourDAO tourDao = new TourDAO();
		Tour tour = tourDao.search(id);
		return tour;
	}
	
	public static boolean update(int id, int discount, int hot) throws PoolException, DAOException {
		TourDAO tourDao = new TourDAO();
		if (tourDao.update(id, discount, hot)) {
			return true;
		} else {
			return false;
		}
	}
}
