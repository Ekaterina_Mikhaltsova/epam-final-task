package com.epam.meo.finalTask.logic;

import com.epam.meo.finalTask.dao.ClientDAO;
import com.epam.meo.finalTask.dao.UserDAO;
import com.epam.meo.finalTask.exception.DAOException;
import com.epam.meo.finalTask.exception.PoolException;
import com.epam.meo.finalTask.resource.ConfigurationManager;

public class RegistrationLogic {
	
	private static String link;
	
	public static boolean checkInfo(String login, String pass, String passRepeat) throws DAOException, 
			PoolException {
		UserDAO userdao = new UserDAO();
		if (userdao.check(login) && pass.equals(passRepeat)) {
			link = ConfigurationManager.getProperty("path.page.main.client");
			return true;
		} else {
			return false;		
		}
	}
	
	public static boolean addUser(String login, String pass) throws DAOException, PoolException {
		UserDAO userdao = new UserDAO();
		ClientDAO clientDao = new ClientDAO();
		if (userdao.add(login, pass) && clientDao.add(userdao.search(login))) {
			return true;
		} else {
			return false;
		}			
	}
	
	public static String getLink() {
		return link;
	}
}
