package com.epam.meo.finalTask.logic;

import com.epam.meo.finalTask.dao.ClientDAO;
import com.epam.meo.finalTask.exception.DAOException;
import com.epam.meo.finalTask.exception.PoolException;

public class ClientLogic {

	public static int search(String name) throws DAOException, PoolException {
		ClientDAO clientDao = new ClientDAO();
		return clientDao.search(name);
	}
}
