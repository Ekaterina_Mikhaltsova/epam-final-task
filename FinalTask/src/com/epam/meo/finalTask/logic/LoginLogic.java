package com.epam.meo.finalTask.logic;

import com.epam.meo.finalTask.dao.UserDAO;
import com.epam.meo.finalTask.entity.User;
import com.epam.meo.finalTask.exception.DAOException;
import com.epam.meo.finalTask.exception.PoolException;
import com.epam.meo.finalTask.resource.ConfigurationManager;

public class LoginLogic {
	
	private static String link;
	
	public static boolean checkLogin(String enterLogin, String enterPass) throws DAOException, PoolException {
		UserDAO userdao = new UserDAO();
		User user = userdao.read(enterLogin);
        switch (user.getRole()) {
		case 2:
			link = ConfigurationManager.getProperty("path.page.main.manager");
			break;

		case 3:
			link = ConfigurationManager.getProperty("path.page.main.client");
			break;

		default:
			break;
		}
        return user.getPassword().equals(enterPass);
	}
	
	public static String getLink() {
		return link;
	}
}
