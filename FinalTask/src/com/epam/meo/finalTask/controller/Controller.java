package com.epam.meo.finalTask.controller;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.PropertyConfigurator;

import com.epam.meo.finalTask.command.ActionCommand;
import com.epam.meo.finalTask.command.factory.ActionFactory;
import com.epam.meo.finalTask.exception.PoolException;
import com.epam.meo.finalTask.resource.ConfigurationManager;
import com.epam.meo.finalTask.wrapper.SessionWrapper;


/**
 * Class-controller, extending {@link HttpServlet}.
 * <p>
 * Organizes work with incoming information from .jsp pages.
 * </p>
 * 
 * @author Kate
 *
 */
@WebServlet("/controller")
public class Controller extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private ActionFactory client;

	public void init() {
		client = new ActionFactory();
		PropertyConfigurator.configure(ConfigurationManager.getProperty("log4j.path"));
	}

	/**
     * Gets information from .jsp pages, passes information to class, implementing patters command, gets processed 
     * information and link to the needed page, then sends received information to the .jsp page.
     * 
     * @throws ServletException
     * @throws IOException
     */
	public void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String page = null;
		SessionWrapper sessionWrapper = new SessionWrapper();
		sessionWrapper.unpacking(request);		
		ActionCommand command = client.defineCommand(sessionWrapper);
		page = command.execute(sessionWrapper);
		if (page != null) {
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
			request = sessionWrapper.updateRequest(request);
			dispatcher.forward(request, response);
		} else {
			page = ConfigurationManager.getProperty("path.page.error");
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
			request = sessionWrapper.updateRequest(request);
			dispatcher.forward(request, response);
		}
	}	
}
