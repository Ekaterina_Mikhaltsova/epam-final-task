package com.epam.meo.finalTask.tag;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.log4j.Logger;

public class CustomDayTag extends TagSupport{
	
	private String format;
	
	private Logger logger = Logger.getLogger(this.getClass());
    
    public void setFormat(String format) {
        this.format = format;
    }	
    
    public int doStartTag() {
        try {
            JspWriter out = pageContext.getOut();
            Date today = new Date();
            SimpleDateFormat dateFormatter = new SimpleDateFormat(format);
            out.print("Today is " + dateFormatter.format(today));
            
        } catch(IOException e) {
        	logger.error(e.getMessage());
        }       
        return EVAL_BODY_INCLUDE;
    }
    
    
    public int doEndTag() {
        return EVAL_PAGE;
    }
}
