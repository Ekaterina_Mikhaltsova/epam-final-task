package com.epam.meo.finalTask.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import com.epam.meo.finalTask.connection.ConnectionPool;
import com.epam.meo.finalTask.entity.Order;
import com.epam.meo.finalTask.exception.DAOException;
import com.epam.meo.finalTask.exception.PoolException;

public class OrderDAO extends AbstractDAO<Order> {
	
	private static ResourceBundle resource = ResourceBundle.getBundle("com.epam.meo.finalTask.dao.requests.order");
	
	private static final String SQL_INSERT_NEW_ORDER = resource.getString("insert.new");
	
	private static final String SQL_SELECT_ALL_ORDERS = resource.getString("select.all");
	
	private static final String SQL_SELECT_ORDER_BY_CLIENT_ID = resource.getString("select.order.client.id");
	
	private static final String SQL_DELETE_ORDER = resource.getString("delete.order");
	
	public OrderDAO() throws PoolException {
		super();
	}

	public boolean add(int tourId, int clientId) throws DAOException, PoolException {
		boolean result = true;
        PreparedStatement ps = null;
        try {
    		connection = ConnectionPool.getInstance().getConnection();
            ps = connection.prepareStatement(SQL_INSERT_NEW_ORDER);
            ps.setString(1, "to be set");
            ps.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
            ps.setInt(3, 1);
            ps.setInt(4, clientId);
            ps.setInt(5, tourId);
            ps.setInt(6, 1);
            int count = 0;
            count = ps.executeUpdate();
            if (count == 0) {
                result = false;
            }
        } catch (SQLException e) {  
        	DAOException dao = new DAOException(e.getMessage());
            dao.setPropertyMessage("error.dao.sqlexception");
            throw dao;
        } finally {
        	try {
				ConnectionPool.getInstance().closeConnection(connection);
			} catch (SQLException e) {
				PoolException pool = new PoolException(e.getMessage());
	            pool.setPropertyMessage("error.pool.exception");
	            throw pool;
			}
        }
        return result;
	}
	
	public List<Order> readAll() throws DAOException, PoolException {
        Statement st = null;
        List<Order> orders = new ArrayList<Order>();
        try {
        	connection = ConnectionPool.getInstance().getConnection();
            st = connection.createStatement();            
            ResultSet resultSet = st.executeQuery(SQL_SELECT_ALL_ORDERS);
            while (resultSet.next()) {
            	Order order = new Order();
            	order.setId(resultSet.getInt("id"));
            	orders.add(order);
            }
        } catch (SQLException e) {
        	DAOException dao = new DAOException(e.getMessage());
            dao.setPropertyMessage("error.dao.sqlexception");
            throw dao;
        } finally {
        	try {
				ConnectionPool.getInstance().closeConnection(connection);
			} catch (SQLException e) {
				PoolException pool = new PoolException(e.getMessage());
	            pool.setPropertyMessage("error.pool.exception");
	            throw pool;
			}       	
        }
        return orders;
    }
	
	public List<Order> find(int clientId) throws DAOException, PoolException {
		PreparedStatement ps = null;
        List<Order> orders = new ArrayList<Order>();
        try {
        	connection = ConnectionPool.getInstance().getConnection();
        	ps = connection.prepareStatement(SQL_SELECT_ORDER_BY_CLIENT_ID); 
        	ps.setInt(1, clientId);            
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
            	Order order = new Order();
            	order.setId(resultSet.getInt("id"));
            	order.setContractNumber(resultSet.getString("contractNumber"));
            	order.setDate(resultSet.getDate("date"));
            	order.setStatus(resultSet.getString("statusname"));
            	order.setTourId(resultSet.getInt("tour_id"));
            	order.setManager(resultSet.getString("name"));
            	orders.add(order);
            }
        } catch (SQLException e) {
        	DAOException dao = new DAOException(e.getMessage());
            dao.setPropertyMessage("error.dao.sqlexception");
            throw dao;
        } finally {
        	try {
				ConnectionPool.getInstance().closeConnection(connection);
			} catch (SQLException e) {
				PoolException pool = new PoolException(e.getMessage());
	            pool.setPropertyMessage("error.pool.exception");
	            throw pool;
			}       	
        }
        return orders;
    }
	
	public boolean delete(int orderId) throws DAOException, PoolException {
		boolean result = true;
        PreparedStatement ps = null;
        try {
    		connection = ConnectionPool.getInstance().getConnection();
            ps = connection.prepareStatement(SQL_DELETE_ORDER);
            ps.setInt(1, orderId);
            int count = 0;
            count = ps.executeUpdate();
            if (count == 0) {
                result = false;
            }
        } catch (SQLException e) {  
        	DAOException dao = new DAOException(e.getMessage());
            dao.setPropertyMessage("error.dao.sqlexception");
            throw dao;
        } finally {
        	try {
				ConnectionPool.getInstance().closeConnection(connection);
			} catch (SQLException e) {
				PoolException pool = new PoolException(e.getMessage());
	            pool.setPropertyMessage("error.pool.exception");
	            throw pool;
			}
        }
        return result;
	}
	
	@Override
	public Order read(int id) {
		// TODO Auto-generated method stub
		return null;
	}
}
