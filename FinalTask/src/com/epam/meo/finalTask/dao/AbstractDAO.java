package com.epam.meo.finalTask.dao;

import java.sql.Connection;
import java.sql.SQLException;

import com.epam.meo.finalTask.connection.ConnectionPool;
import com.epam.meo.finalTask.entity.Entity;
import com.epam.meo.finalTask.exception.PoolException;

public abstract class AbstractDAO<T extends Entity> {
	
	protected Connection connection;
	
	public AbstractDAO() throws PoolException {
        try {
            this.connection = ConnectionPool.getInstance().getConnection();
        } catch (SQLException e) {
            PoolException pool = new PoolException(e.getMessage());
            pool.setPropertyMessage("error.pool.exception");
            throw pool;
        }        
    }
	
	public abstract T read(int id);
}
