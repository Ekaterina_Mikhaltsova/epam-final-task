package com.epam.meo.finalTask.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import com.epam.meo.finalTask.connection.ConnectionPool;
import com.epam.meo.finalTask.entity.Tour;
import com.epam.meo.finalTask.exception.DAOException;
import com.epam.meo.finalTask.exception.PoolException;

public class TourDAO extends AbstractDAO<Tour> {
	
	private static ResourceBundle resource = ResourceBundle.getBundle("com.epam.meo.finalTask.dao.requests.tour");
	
	private static final String SQL_SELECT_ALL_TOURS = resource.getString("select.all");
	
	private static final String SQL_SELECT_TOUR_BY_ID = resource.getString("select.tour.id");
	
	private static final String SQL_UPDATE_TOUR_BY_ID = resource.getString("update.tour.id");
	
	public TourDAO() throws PoolException {
		super();
	}
	
	public List<Tour> readAll() throws DAOException, PoolException {
        Statement st = null;
        List<Tour> tours = new ArrayList<Tour>();
        try {
        	connection = ConnectionPool.getInstance().getConnection();
            st = connection.createStatement();            
            ResultSet resultSet = st.executeQuery(SQL_SELECT_ALL_TOURS);
            while (resultSet.next()) {
            	Tour tour = new Tour();
            	tour.setId(resultSet.getInt("id"));
            	tour.setPrice(resultSet.getDouble("price"));
            	tour.setDate(resultSet.getDate("date"));
            	tour.setLength(resultSet.getInt("length"));
            	tour.setDiscount(resultSet.getInt("discount"));
            	tour.setHot(resultSet.getInt("hot"));
            	tour.setType(resultSet.getString("name"));
            	tours.add(tour);
            }
        } catch (SQLException e) {
        	DAOException dao = new DAOException(e.getMessage());
            dao.setPropertyMessage("error.dao.sqlexception");
            throw dao;
        } finally {
        	try {
				ConnectionPool.getInstance().closeConnection(connection);
			} catch (SQLException e) {
				PoolException pool = new PoolException(e.getMessage());
	            pool.setPropertyMessage("error.pool.exception");
	            throw pool;
			}      	
        }
        return tours;
    }
	
	public Tour search(int tourId) throws PoolException, DAOException {
		PreparedStatement ps = null;
        Tour tour = new Tour();
        try {
        	connection = ConnectionPool.getInstance().getConnection();
        	ps = connection.prepareStatement(SQL_SELECT_TOUR_BY_ID);
        	ps.setInt(1, tourId);            
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
            	tour.setId(resultSet.getInt("id"));
            	tour.setPrice(resultSet.getDouble("price"));
            	tour.setDate(resultSet.getDate("date"));
            	tour.setLength(resultSet.getInt("length"));
            	tour.setDiscount(resultSet.getInt("discount"));
            	tour.setHot(resultSet.getInt("hot"));
            	tour.setType(resultSet.getString("name"));
            }
        } catch (SQLException e) {
        	DAOException dao = new DAOException(e.getMessage());
            dao.setPropertyMessage("error.dao.sqlexception");
            throw dao;
        } finally {
        	try {
				ConnectionPool.getInstance().closeConnection(connection);
			} catch (SQLException e) {
				PoolException pool = new PoolException(e.getMessage());
	            pool.setPropertyMessage("error.pool.exception");
	            throw pool;
			}      	
        }
        return tour;
	}
	
	public boolean update(int id, int discount, int hot) throws DAOException, PoolException {
		boolean result = true;
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(SQL_UPDATE_TOUR_BY_ID);
            ps.setInt(1, discount);
            ps.setInt(2, hot);
            ps.setInt(3, id);
            int count = 0;
            count = ps.executeUpdate();
            if (count == 0) {
                result = false;
            }
        } catch (SQLException e) {
        	DAOException dao = new DAOException(e.getMessage());
            dao.setPropertyMessage("error.dao.sqlexception");
            throw dao;
        } finally {
        	try {
				ConnectionPool.getInstance().closeConnection(connection);
			} catch (SQLException e) {
				PoolException pool = new PoolException(e.getMessage());
	            pool.setPropertyMessage("error.pool.exception");
	            throw pool;
			}      	
        }
        return result;
	}
	
	@Override
	public Tour read(int id) {
		// TODO Auto-generated method stub
		return null;
	}

}
