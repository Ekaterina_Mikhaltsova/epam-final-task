package com.epam.meo.finalTask.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import com.epam.meo.finalTask.connection.ConnectionPool;
import com.epam.meo.finalTask.entity.Client;
import com.epam.meo.finalTask.exception.DAOException;
import com.epam.meo.finalTask.exception.PoolException;

public class ClientDAO extends AbstractDAO<Client> {

	private static ResourceBundle resource = ResourceBundle.getBundle("com.epam.meo.finalTask.dao.requests.client");
	
	private static final String SQL_INSERT_NEW_CLIENT = resource.getString("insert.new");
	
	private static final String SQL_SELECT_ID_BY_LOGIN = resource.getString("select.id.login");
	
	public ClientDAO() throws PoolException {
		super();
	}
	
	public boolean add(int id) throws DAOException, PoolException {
		boolean result = true;
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(SQL_INSERT_NEW_CLIENT);
            ps.setInt(1, id);
            int count = 0;
            count = ps.executeUpdate();
            if (count == 0) {
                result = false;
            }
        } catch (SQLException e) {  
        	DAOException dao = new DAOException(e.getMessage());
            dao.setPropertyMessage("error.dao.sqlexception");
            throw dao;
        } finally {
        	try {
				ConnectionPool.getInstance().closeConnection(connection);
			} catch (SQLException e) {
				PoolException pool = new PoolException(e.getMessage());
	            pool.setPropertyMessage("error.pool.exception");
	            throw pool;
			}
        }
        return result;
	}
	
	public int search(String name) throws DAOException, PoolException {
		PreparedStatement ps = null;
        try {
        	connection = ConnectionPool.getInstance().getConnection();
            ps = connection.prepareStatement(SQL_SELECT_ID_BY_LOGIN);
            ps.setString(1, name);
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt("id");
            } else {
            	return 0;
            }
        } catch (SQLException e) {
        	DAOException dao = new DAOException(e.getMessage());
            dao.setPropertyMessage("error.dao.sqlexception");
            throw dao;
        } finally {
        	try {
				ConnectionPool.getInstance().closeConnection(connection);
			} catch (SQLException e) {
				PoolException pool = new PoolException(e.getMessage());
	            pool.setPropertyMessage("error.pool.exception");
	            throw pool;
			}        	
        }		
	}

	@Override
	public Client read(int id) {
		// TODO Auto-generated method stub
		return null;
	}

}
