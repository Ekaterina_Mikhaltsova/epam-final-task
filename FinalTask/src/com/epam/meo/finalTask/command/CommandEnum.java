package com.epam.meo.finalTask.command;

public enum CommandEnum {

	LOGIN {
		{
			this.command = new LoginCommand();
		}
	},
	
	LOGOUT {
		{
			this.command = new LogoutCommand();
		}
	},
	
	REGISTERCHOICE {
		{
			this.command = new RegisterChoiceCommand();
		}
	},
	
	REGISTER {
		{
			this.command = new RegisterCommand();
		}
	},
	
	SHOWTOURS {
		{
			this.command = new ShowToursCommand();
		}
	},
	
	SHOWCLIENTORDERS {
		{
			this.command = new ShowClientOrdersCommand();
		}
	},
	
	BUY {
		{
			this.command = new BuyCommand();
		}
	},
	
	DELETEORDER {
		{
			this.command = new DeleteOrderCommand();
		}
	},
	
	EDITTOURS {
		{
			this.command = new EditToursCommand();
		}
	},
	
	EDITTOURSCHOICE {
		{
			this.command = new EditToursChoiceCommand();
		}
	},
	
	MAINCLIENT {
		{
			this.command = new MainClientCommand();
		}
	},
	
	SUBMITEDIT {
		{
			this.command = new SubmitEditCommand();
		}
	};	
	
	ActionCommand command;
	public ActionCommand getCurrentCommand() {
		return command;
	}
}
