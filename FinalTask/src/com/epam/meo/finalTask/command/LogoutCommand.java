package com.epam.meo.finalTask.command;

import com.epam.meo.finalTask.resource.ConfigurationManager;
import com.epam.meo.finalTask.wrapper.SessionWrapper;

public class LogoutCommand implements ActionCommand {

	@Override
	public String execute(SessionWrapper sessionWrapper) {
		sessionWrapper.removeSession();
		return ConfigurationManager.getProperty("path.page.login");
	}
}
