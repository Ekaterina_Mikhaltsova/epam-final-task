package com.epam.meo.finalTask.command;

import com.epam.meo.finalTask.resource.ConfigurationManager;
import com.epam.meo.finalTask.wrapper.SessionWrapper;

public class EmptyCommand implements ActionCommand {

	@Override
	public String execute(SessionWrapper sessionWrapper) {
		String page = ConfigurationManager.getProperty("path.page.error");
		return page;
	}
}
