package com.epam.meo.finalTask.command;

import com.epam.meo.finalTask.resource.ConfigurationManager;
import com.epam.meo.finalTask.wrapper.SessionWrapper;

public class MainClientCommand implements ActionCommand {

	@Override
	public String execute(SessionWrapper sessionWrapper) {
		return ConfigurationManager.getProperty("path.page.main.client");
	}
}
