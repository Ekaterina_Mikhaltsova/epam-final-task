package com.epam.meo.finalTask.command;

import org.apache.log4j.Logger;

import com.epam.meo.finalTask.exception.DAOException;
import com.epam.meo.finalTask.exception.PoolException;
import com.epam.meo.finalTask.logic.OrderLogic;
import com.epam.meo.finalTask.resource.ConfigurationManager;
import com.epam.meo.finalTask.wrapper.SessionWrapper;

public class DeleteOrderCommand implements ActionCommand {
	
	private Logger logger = Logger.getLogger(this.getClass());

	@Override
	public String execute(SessionWrapper sessionWrapper) {
		String page = null;
		try {
			int id = Integer.parseInt(sessionWrapper.getParameter("orderid"));
			if (OrderLogic.delete(id)) {
				page = ConfigurationManager.getProperty("path.page.main.client");
			} else {
				page = ConfigurationManager.getProperty("path.page.error");
			}
		} catch (DAOException e) {
			page = ConfigurationManager.getProperty("path.page.error");	
			sessionWrapper.setAttribute("error", e.getMessage());
			sessionWrapper.setAttribute("property", e.getPropertyMessage());
			logger.error(e.getMessage());
		} catch (PoolException e) {
			page = ConfigurationManager.getProperty("path.page.error");	
			sessionWrapper.setAttribute("error", e.getMessage());
			sessionWrapper.setAttribute("property", e.getPropertyMessage());
			logger.error(e.getMessage());
		}
		return page;
	}

}
