package com.epam.meo.finalTask.command;

import java.util.List;

import org.apache.log4j.Logger;

import com.epam.meo.finalTask.entity.Order;
import com.epam.meo.finalTask.exception.DAOException;
import com.epam.meo.finalTask.exception.PoolException;
import com.epam.meo.finalTask.logic.OrderLogic;
import com.epam.meo.finalTask.resource.ConfigurationManager;
import com.epam.meo.finalTask.wrapper.SessionWrapper;

public class ShowClientOrdersCommand implements ActionCommand {

	private Logger logger = Logger.getLogger(this.getClass());
	
	@Override
	public String execute(SessionWrapper sessionWrapper) {
		String page = ConfigurationManager.getProperty("path.page.show.bought.tours");
		List<Order> orders;
		try {
			orders = OrderLogic.showOrders(Integer.parseInt(sessionWrapper.getSessionAttribute("clientId").toString()));
			sessionWrapper.setAttribute("orders", orders);
		} catch (DAOException e) {
			page = ConfigurationManager.getProperty("path.page.error");	
			sessionWrapper.setAttribute("error", e.getMessage());
			sessionWrapper.setAttribute("property", e.getPropertyMessage());
			logger.error(e.getMessage());
		} catch (PoolException e) {
			page = ConfigurationManager.getProperty("path.page.error");	
			sessionWrapper.setAttribute("error", e.getMessage());
			sessionWrapper.setAttribute("property", e.getPropertyMessage());
			logger.error(e.getMessage());
		}
		return page;
	}

}
