package com.epam.meo.finalTask.command;

import org.apache.log4j.Logger;

import com.epam.meo.finalTask.entity.Tour;
import com.epam.meo.finalTask.exception.DAOException;
import com.epam.meo.finalTask.exception.PoolException;
import com.epam.meo.finalTask.logic.TourLogic;
import com.epam.meo.finalTask.resource.ConfigurationManager;
import com.epam.meo.finalTask.wrapper.SessionWrapper;

public class EditToursCommand implements ActionCommand {

	private Logger logger = Logger.getLogger(this.getClass());
	
	@Override
	public String execute(SessionWrapper sessionWrapper) {
		String page = ConfigurationManager.getProperty("path.page.edit.chosen.tour");
		try {
			int id = Integer.parseInt(sessionWrapper.getParameter("tourid"));
			Tour tour = TourLogic.search(id);
			sessionWrapper.setSessionAttribute("tour", tour);
		} catch (DAOException e) {
			page = ConfigurationManager.getProperty("path.page.error");	
			sessionWrapper.setAttribute("error", e.getMessage());
			sessionWrapper.setAttribute("property", e.getPropertyMessage());
			logger.error(e.getMessage());
		} catch (PoolException e) {
			page = ConfigurationManager.getProperty("path.page.error");	
			sessionWrapper.setAttribute("error", e.getMessage());
			sessionWrapper.setAttribute("property", e.getPropertyMessage());
			logger.error(e.getMessage());
		}
		return page;
	}
}
