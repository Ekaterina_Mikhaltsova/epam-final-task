package com.epam.meo.finalTask.command;

import org.apache.log4j.Logger;

import com.epam.meo.finalTask.exception.DAOException;
import com.epam.meo.finalTask.exception.PoolException;
import com.epam.meo.finalTask.logic.RegistrationLogic;
import com.epam.meo.finalTask.resource.ConfigurationManager;
import com.epam.meo.finalTask.resource.MessageManager;
import com.epam.meo.finalTask.wrapper.SessionWrapper;

public class RegisterCommand implements ActionCommand {

	private static final String PARAM_NAME_LOGIN = "login";
	private static final String PARAM_NAME_PASSWORD = "password";
	private static final String PARAM_NAME_PASSWORDREPEAT = "passwordRepeat";
	
	private Logger logger = Logger.getLogger(this.getClass());
	
	@Override
	public String execute(SessionWrapper sessionWrapper) {
		String page = null;
		String login = sessionWrapper.getParameter(PARAM_NAME_LOGIN);
		String pass = sessionWrapper.getParameter(PARAM_NAME_PASSWORD);
		String passRepeat = sessionWrapper.getParameter(PARAM_NAME_PASSWORDREPEAT);
		try {
			if (login == "" || pass == "" || passRepeat == "") {
				sessionWrapper.setAttribute("errorRegistrationMessage",
						MessageManager.getProperty("message.registrationError"));
				page = ConfigurationManager.getProperty("path.page.registration");
			} else {
				if (RegistrationLogic.checkInfo(login, pass, passRepeat) && 
						RegistrationLogic.addUser(login, pass)) {
					sessionWrapper.setAttribute("user", login);
					page = RegistrationLogic.getLink();
				} else {
					sessionWrapper.setAttribute("errorRegistrationMessage",
							MessageManager.getProperty("message.registrationError"));
					page = ConfigurationManager.getProperty("path.page.registration");
				}
			}
		} catch (DAOException e) {
			page = ConfigurationManager.getProperty("path.page.error");	
			sessionWrapper.setAttribute("error", e.getMessage());
			sessionWrapper.setAttribute("property", e.getPropertyMessage());
			logger.error(e.getMessage());
		} catch (PoolException e) {
			page = ConfigurationManager.getProperty("path.page.error");	
			sessionWrapper.setAttribute("error", e.getMessage());
			sessionWrapper.setAttribute("property", e.getPropertyMessage());
			logger.error(e.getMessage());
		}
		return page;
	}

}
