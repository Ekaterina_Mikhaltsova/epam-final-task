package com.epam.meo.finalTask.command.factory;

import org.apache.log4j.Logger;

import com.epam.meo.finalTask.command.ActionCommand;
import com.epam.meo.finalTask.command.CommandEnum;
import com.epam.meo.finalTask.command.EmptyCommand;
import com.epam.meo.finalTask.wrapper.SessionWrapper;

/**
 * Class-factory for incoming commands.
 * <p>
 * A factory to create a needed action.
 * </p>
 * 
 * @author Kate
 *
 */
public class ActionFactory {
	
	private Logger logger = Logger.getLogger(this.getClass());
		
	/**
     * Defines the incoming command and creates an action.
     * 
     * @param sessionWrapper  SessionWrapper object holding all session information
     */
	public ActionCommand defineCommand(SessionWrapper sessionWrapper) {
		ActionCommand current = new EmptyCommand();
		String action = sessionWrapper.getParameter("command");
		if (action == null || action.isEmpty()) {
			return current;
		}
		try {
			CommandEnum currentEnum = CommandEnum.valueOf(action.toUpperCase());
			current = currentEnum.getCurrentCommand();
		} catch (IllegalArgumentException e) {
			logger.error(e.getMessage());
		}
		return current;
	}
}
