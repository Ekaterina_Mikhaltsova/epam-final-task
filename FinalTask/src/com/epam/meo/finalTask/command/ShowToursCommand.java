package com.epam.meo.finalTask.command;

import java.util.List;

import org.apache.log4j.Logger;

import com.epam.meo.finalTask.entity.Tour;
import com.epam.meo.finalTask.exception.DAOException;
import com.epam.meo.finalTask.exception.PoolException;
import com.epam.meo.finalTask.logic.TourLogic;
import com.epam.meo.finalTask.resource.ConfigurationManager;
import com.epam.meo.finalTask.wrapper.SessionWrapper;

public class ShowToursCommand implements ActionCommand {

	private Logger logger = Logger.getLogger(this.getClass());
	
	@Override
	public String execute(SessionWrapper sessionWrapper) {
		String page = ConfigurationManager.getProperty("path.page.show.tours");
		List<Tour> tours;
		try {
			tours = TourLogic.showTours();
			sessionWrapper.setAttribute("tours", tours);
		} catch (DAOException e) {
			page = ConfigurationManager.getProperty("path.page.error");	
			sessionWrapper.setAttribute("error", e.getMessage());
			sessionWrapper.setAttribute("property", e.getPropertyMessage());
			logger.error(e.getMessage());
		} catch (PoolException e) {
			page = ConfigurationManager.getProperty("path.page.error");	
			sessionWrapper.setAttribute("error", e.getMessage());
			sessionWrapper.setAttribute("property", e.getPropertyMessage());
			logger.error(e.getMessage());
		}
		return page;
	}
}
