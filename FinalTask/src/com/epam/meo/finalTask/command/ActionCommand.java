package com.epam.meo.finalTask.command;

import com.epam.meo.finalTask.wrapper.SessionWrapper;

public interface ActionCommand {

	String execute(SessionWrapper sessionWrapper);
}
