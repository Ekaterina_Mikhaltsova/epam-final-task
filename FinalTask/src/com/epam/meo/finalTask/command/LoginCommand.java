package com.epam.meo.finalTask.command;

import org.apache.log4j.Logger;

import com.epam.meo.finalTask.exception.DAOException;
import com.epam.meo.finalTask.exception.PoolException;
import com.epam.meo.finalTask.logic.ClientLogic;
import com.epam.meo.finalTask.logic.LoginLogic;
import com.epam.meo.finalTask.resource.ConfigurationManager;
import com.epam.meo.finalTask.resource.MessageManager;
import com.epam.meo.finalTask.wrapper.SessionWrapper;

public class LoginCommand implements ActionCommand {

	private static final String PARAM_NAME_LOGIN = "login";
	private static final String PARAM_NAME_PASSWORD = "password";
	
	private Logger logger = Logger.getLogger(this.getClass());

	@Override
	public String execute(SessionWrapper sessionWrapper) {		
		String page = null;
		String login = sessionWrapper.getParameter(PARAM_NAME_LOGIN);
		String pass = sessionWrapper.getParameter(PARAM_NAME_PASSWORD);
		try {
			if (login == "" || pass == "") {
				sessionWrapper.setAttribute("errorLoginPassMessage",
						MessageManager.getProperty("message.loginerror"));
				page = ConfigurationManager.getProperty("path.page.login");
			} else {
				if (LoginLogic.checkLogin(login, pass)) {
					sessionWrapper.setSessionAttribute("user", login);
					int clientId = ClientLogic.search(login);
					sessionWrapper.setSessionAttribute("clientId", clientId);
					page = LoginLogic.getLink();
				} else {
					sessionWrapper.setAttribute("errorLoginPassMessage",
							MessageManager.getProperty("message.loginerror"));
					page = ConfigurationManager.getProperty("path.page.login");			
				}
			}
		} catch (DAOException e) {
			page = ConfigurationManager.getProperty("path.page.error");	
			sessionWrapper.setAttribute("error", e.getMessage());
			sessionWrapper.setAttribute("property", e.getPropertyMessage());
			logger.error(e.getMessage());
		} catch (PoolException e) {
			page = ConfigurationManager.getProperty("path.page.error");	
			sessionWrapper.setAttribute("error", e.getMessage());
			sessionWrapper.setAttribute("property", e.getPropertyMessage());
			logger.error(e.getMessage());
		}
		return page;
	}
}
