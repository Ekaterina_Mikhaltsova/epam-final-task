package com.epam.meo.finalTask.command;

import org.apache.log4j.Logger;

import com.epam.meo.finalTask.exception.DAOException;
import com.epam.meo.finalTask.exception.PoolException;
import com.epam.meo.finalTask.logic.TourLogic;
import com.epam.meo.finalTask.resource.ConfigurationManager;
import com.epam.meo.finalTask.wrapper.SessionWrapper;

public class SubmitEditCommand implements ActionCommand {

	private Logger logger = Logger.getLogger(this.getClass());
	
	@Override
	public String execute(SessionWrapper sessionWrapper) {
		String page = null;
		int discount = Integer.parseInt(sessionWrapper.getParameter("discount"));
		int hot = Integer.parseInt(sessionWrapper.getParameter("hot"));
		int tourId = Integer.parseInt(sessionWrapper.getParameter("tourid"));
		try {
			if (TourLogic.update(tourId, discount, hot)) {
				page = ConfigurationManager.getProperty("path.page.edit.success");
			} else {
				page = ConfigurationManager.getProperty("path.page.edit.tours");
			}
		} catch (DAOException e) {
			page = ConfigurationManager.getProperty("path.page.error");	
			sessionWrapper.setAttribute("error", e.getMessage());
			sessionWrapper.setAttribute("property", e.getPropertyMessage());
			logger.error(e.getMessage());
		} catch (PoolException e) {
			page = ConfigurationManager.getProperty("path.page.error");	
			sessionWrapper.setAttribute("error", e.getMessage());
			sessionWrapper.setAttribute("property", e.getPropertyMessage());
			logger.error(e.getMessage());
		}
		return page;
	}

}
