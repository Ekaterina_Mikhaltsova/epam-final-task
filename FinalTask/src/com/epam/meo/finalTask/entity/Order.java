package com.epam.meo.finalTask.entity;

import java.util.Date;

public class Order extends Entity {

	private String contractNumber;
	private Date date;
	private String status;
	private int clientId;
	private int tourId;
	private String manager;
	
	public Order() {
		super();
		this.contractNumber = "";
		this.date = null;
		this.status = "";
		this.clientId = 0;
		this.tourId = 0;
		this.manager = "";
	}
	
	public Order(int id, int clientId, int tourId, String status, String manager, Date date, 
			String contractNumber) {
        super(id);
        this.contractNumber = contractNumber;
		this.date = date;
		this.status = status;
        this.clientId = clientId;
		this.tourId = tourId;
		this.manager = manager;
	}

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public int getTourId() {
		return tourId;
	}

	public void setTourId(int tourId) {
		this.tourId = tourId;
	}

	public String getManager() {
		return manager;
	}

	public void setManager(String manager) {
		this.manager = manager;
	}
}
