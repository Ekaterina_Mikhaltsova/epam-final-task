package com.epam.meo.finalTask.entity;

import java.util.Date;

public class Tour extends Entity {

	private double price;
	private Date date;
	private int length;
	private int discount;
	private int hot;
	private int tourOperatorId;
	private String type;
	
	public Tour() {
		super();
		this.price = 0;
		this.date = null;
		this.length = 0;
		this.discount = 0;
		this.hot = 0;
		this.tourOperatorId = 0;
		this.type = null;
	}
	
	public Tour(int id, double price, Date date, int length, int discount, int hot,
			int tourOperatorId, String type) {
        super(id);
        this.price = price;
		this.date = date;
		this.length = length;
		this.discount = discount;
		this.hot = hot;
		this.tourOperatorId = tourOperatorId;
		this.type = type;
	}
	
	public String isHot() {
		if (this.hot == 1) {
			return "Yes";
		} else {
			return "No";
		}
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getDiscount() {
		return discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}

	public int getHot() {
		return hot;
	}

	public void setHot(int hot) {
		this.hot = hot;
	}

	public int getTourOperatorId() {
		return tourOperatorId;
	}

	public void setTourOperatorId(int tourOperatorId) {
		this.tourOperatorId = tourOperatorId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
