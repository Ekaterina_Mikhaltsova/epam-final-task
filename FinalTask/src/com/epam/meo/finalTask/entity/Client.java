package com.epam.meo.finalTask.entity;

public class Client extends User {
 
	private String phone;
	private String passportData;
	
	public Client() {
        super();
        this.phone = "";
        this.passportData = "";
    }

    public Client(int id, String login, String password, String name, String surname,
    		int role, String phone, String passportData) {
        super(id, login, password, name, surname, role);
        this.phone = phone;
        this.passportData = passportData;
    }

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPassportData() {
		return passportData;
	}

	public void setPassportData(String passportData) {
		this.passportData = passportData;
	}
}
